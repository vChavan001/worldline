const { log } = require('console')
const express = require('express')
const app = express()
const port = 8019

app.get('/',(req,res)=>{
    res.send('This is a sample for CICD piplie')
})

app.listen(port,()=>{
    console.log("Application is listing at http://localhost:${port}")
})